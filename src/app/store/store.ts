import { RankingsApi } from "./services/rankings";
import { configureStore } from "@reduxjs/toolkit";
// Or from '@reduxjs/toolkit/query/react'
import { setupListeners } from "@reduxjs/toolkit/query";
import { heroesApi } from "./services/heroes";
import { benchApi } from "./services/bench";
import { heroesDetailApi } from "./services/heroesDetail";
import { searchApi } from "./services/players";
import heroes from "./slices/heroes";
import players from "./slices/players";
export const store = configureStore({
  reducer: {
    // Add the generated reducer as a specific top-level slice
    [heroesApi.reducerPath]: heroesApi.reducer,
    [heroesDetailApi.reducerPath]: heroesDetailApi.reducer,
    [RankingsApi.reducerPath]: RankingsApi.reducer,
    [benchApi.reducerPath]: benchApi.reducer,
    [searchApi.reducerPath]: searchApi.reducer,
    heroes,
    players,
  },
  // Adding the api middleware enables caching, invalidation, polling,
  // and other useful features of `rtk-query`.
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      heroesApi.middleware,
      heroesDetailApi.middleware,
      RankingsApi.middleware,
      benchApi.middleware,
      searchApi.middleware
    ),
});

// optional, but required for refetchOnFocus/refetchOnReconnect behaviors
// see `setupListeners` docs - takes an optional callback as the 2nd arg for customization
setupListeners(store.dispatch);
export type AppDispatch = typeof store.dispatch;
