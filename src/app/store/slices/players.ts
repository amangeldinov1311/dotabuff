import { IHeroStats, Match } from "./../../../types";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const fetchPlayers = createAsyncThunk(
  "players/fetchPlayersStatus",
  async (term: any) => {
    const { data } = await axios.get(
      `https://api.opendota.com/api/search?q=${term}`
    );
    return data;
  }
);
const initialState = {
  items: [],
  loading: true,
};

const PlayersSlice = createSlice({
  name: "players",
  initialState,
  reducers: {
    setItems(state, action) {
      state.items = action.payload;
    },
  },
  extraReducers: (buider) => {
    buider.addCase(fetchPlayers.pending, (state) => {
      console.log(state, "отправка");
      state.loading = true;
    }),
      buider.addCase(fetchPlayers.fulfilled, (state, action) => {
        state.items = action.payload;
        state.loading = false;
        console.log(state, "ok");
      }),
      buider.addCase(fetchPlayers.rejected, (state) => {
        console.log(state, "ошибка");
        state.loading = true;
      });
  },
});
export const { setItems } = PlayersSlice.actions;
export default PlayersSlice.reducer;
