import { IHeroStats } from "@/src/types";

import { createSlice } from "@reduxjs/toolkit";

// type State = {
//   items: IHeroStats[];
//   loading: Boolean;
// };
const initialState = {
  items: [],
  loading: true,
};

const HeroesSlice = createSlice({
  name: "heroes",
  initialState,
  reducers: {
    setItems(state, action) {
      state.items = action.payload;
    },
    setLoading(state, action) {
      state.loading = action.payload;
    },
  },
});
export const { setItems, setLoading } = HeroesSlice.actions;
export default HeroesSlice.reducer;
