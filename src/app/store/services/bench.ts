import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { IBench } from "@/src/types";
export const benchApi = createApi({
  reducerPath: "benchApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.opendota.com/api/" }),
  endpoints: (builder) => ({
    getAllBench: builder.query<IBench, number>({
      query: (id) => `benchmarks?hero_id=${id}`,
    }),
  }),
});

export const { useGetAllBenchQuery } = benchApi;
