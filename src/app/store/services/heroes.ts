import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { IHeroStats } from "@/src/types";
export const heroesApi = createApi({
  reducerPath: "pokemonApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.opendota.com/api/" }),
  endpoints: (builder) => ({
    getAllHeroes: builder.query<IHeroStats[], string>({
      query: (id) => `heroStats/${id}`,
    }),
	
  }),
});

export const { useGetAllHeroesQuery } = heroesApi;
