import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { IHeroRank } from "@/src/types";
export const RankingsApi = createApi({
  reducerPath: "rankingsApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.opendota.com/api/" }),
  endpoints: (builder) => ({
    GetRankings: builder.query<IHeroRank, number>({
      query: (id) => `rankings?hero_id=${id}`,
    }),
  }),
});

export const { useGetRankingsQuery } = RankingsApi;
