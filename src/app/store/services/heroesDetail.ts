import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { Durations, IDetailHero } from "@/src/types";
import { Match } from "@/src/types";
export const heroesDetailApi = createApi({
  reducerPath: "heroesDetailApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.opendota.com/api/" }),
  endpoints: (builder) => ({
    GetHeroesDetail: builder.query<IDetailHero[], string>({
      query: (id) => `heroes/${id}`,
    }),
    GetHeroesMatchups: builder.query<Match[], number>({
      query: (id) => `heroes/${id}/matchups`,
    }),
    GetHeroesDurations: builder.query<Durations[], number>({
      query: (id) => `heroes/${id}/durations`,
    }),
  }),
});

export const {
  useGetHeroesDetailQuery,
  useGetHeroesMatchupsQuery,
  useGetHeroesDurationsQuery,
} = heroesDetailApi;
