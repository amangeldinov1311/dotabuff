import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const searchApi = createApi({
  reducerPath: "searchApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.opendota.com/api/" }),
  endpoints: (builder) => ({
    getPlayers: builder.query<any, string>({
      query: (term) => `search?q=${term}`,
    }),
  }),
});

export const { useGetPlayersQuery } = searchApi;
