import React from "react";
type Props = {
  classs?: string;
};
function Star({classs}: Props) {
  return (
    <svg
      className={classs}
      height="800px"
      width="800px"
     
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        d="M414.616,512L256.024,391.104L97.4,512l60.592-195.608L0,196.032h195.264L256.024,0l60.736,196.032
	H512l-157.968,120.36L414.616,512z"
      />
      <g>
        <path d="M256.024,391.104L97.4,512l60.592-195.608L0,196.032h195.264L256.024,0" />
      </g>
    </svg>
  );
}

export default Star;
