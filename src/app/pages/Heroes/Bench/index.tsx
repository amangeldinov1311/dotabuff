import { AreaChart } from "@/src/app/components/charts/area";
import { Result } from "@/src/types";
import React from "react";
import styles from "./styles.module.scss";
type Props = {
  data: Result;
};
function Bench({ data }: Props) {
  return (
    <div className={styles.wrapper}>
      <div className={styles.item}>
        <AreaChart
          data={data.gold_per_min}
          gradient1="transparent"
          gradient2="rgba(253,191,45,.5)"
          color="#7D7529"
        />
        <div className={styles.title}>Заработанное золото в минуту</div>
      </div>
      <div className={styles.item}>
        <AreaChart
          data={data.xp_per_min}
          gradient1="transparent"
          gradient2="rgba(45,140,253,1)"
          color="rgb(102, 187, 255)"
        />
        <div className={styles.title}>Заработанный опыт в минуту</div>
      </div>
      <div className={styles.item}>
        <AreaChart
          data={data.hero_damage_per_min}
          gradient1="transparent"
          gradient2="rgba(253,45,137,1)"
          color="rgb(255, 76, 76)"
        />
        <div className={styles.title}>Урон по героям в минуту</div>
      </div>
      <div className={styles.item}>
        <AreaChart
          data={data.hero_healing_per_min}
          gradient1="transparent"
          gradient2="rgba(45,253,146,1)"
          color="rgb(102, 187, 106)"
        />
        <div className={styles.title}>Лечение героев в минуту</div>
      </div>
      <div className={styles.item}>
        <AreaChart
          data={data.kills_per_min}
          gradient1="transparent"
          gradient2="rgba(252,253,45,1)"
          color="rgb(255, 171, 64)"
        />
        <div className={styles.title}>Заработанный опыт в минуту</div>
      </div>
      <div className={styles.item}>
        <AreaChart
          data={data.last_hits_per_min}
          gradient1="transparent"
          gradient2="rgba(45,253,244,1)"
          color="rgb(124, 153, 168)"
        />
        <div className={styles.title}>Добитые крипы в минуту</div>
      </div>
      <div className={styles.item}>
        <AreaChart
          data={data.lhten}
          gradient1="transparent"
          gradient2="rgba(253,191,45,1)"
          color="#7D7529"
        />
        <div className={styles.title}>Добиваний за первые 10 минут</div>
      </div>
      <div className={styles.item}>
        <AreaChart
          data={data.stuns_per_min}
          gradient1="transparent"
          gradient2="rgba(253,45,137,1)"
          color="rgb(255, 76, 76)"
        />
        <div className={styles.title}>Количество оглушения в минуту</div>
      </div>
    </div>
  );
}

export default Bench;
{
  /* {data.map((el) =>
        el.map((item) => (
          <div className={styles.item}>
            <AreaChart data={item} />
            <div className={styles.title}>Заработанное золото в минуту</div>
          </div>
        ))
      )} */
}
{
  /* {data.map((el) => (
        <div className={styles.item}>
          <AreaChart data={el} />
          <div className={styles.title}>Заработанное золото в минуту</div>
        </div>
      ))} */
}
{
  /* <div className={styles.item}>
          <AreaChart data={data.xp_per_min} />
        </div> */
}
