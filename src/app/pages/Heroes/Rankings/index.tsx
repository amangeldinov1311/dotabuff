import React from "react";
import HeroDetail from "../HeroDetail";
import { IRankings } from "@/src/types";
import styles from "./styles.module.scss";
import ArrowRight from "@/src/app/icons/ArrowRight";
import ProgressBar from "react-bootstrap/ProgressBar";

type Props = {
  data: IRankings[];
};
function Rankings({ data }: Props) {
  const [items, setItems] = React.useState<IRankings[]>(data);
  //   const sortData=()=>{
  // 	items.sort(())
  //   }
  const max = items.reduce((acc, curr) =>
    acc.score > curr.score ? acc : curr
  );
  return (
    <>
      <div className={styles.title}>Ранги</div>
      <div className={styles.table}>
        <div className={styles.top}>
          <div className={styles.row}>
            <div className={styles.col}>
              <div className={styles.td}>Ранг</div>
            </div>
            <div className={styles.col}>
              <div className={styles.td}>Имя</div>
            </div>
            <div className={styles.col}>
              <div className={styles.td}>Счет</div>
            </div>
          </div>
        </div>

        {items &&
          items.map((el, i: number) => (
            <div key={i}>
              <div className={styles.row}>
                <div className={styles.col}>
                  <div className={styles.td}>{i + 1}</div>
                </div>
                <div className={styles.col}>
                  <div className={styles.td}>
                    <div className={styles.player}>
                      <img src={el.avatar} alt="" />
                      <div className={styles.text}>
                        <span className={styles.name}>
                          {el.personaname}
                          <ArrowRight />
                        </span>
                        <span className={styles.rank}>Титан</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={styles.col}>
                  <div className={styles.wraper}>
                    <div className={styles.td}>{Math.round(el.score)}</div>
                    <ProgressBar max={max.score} now={el.score} />
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
    </>
  );
}

export default Rankings;
