import Star from "@/src/app/icons/Star";
import { IHeroStats } from "@/src/types";
import React from "react";
import styles from "./styles.module.scss";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import { setItems } from "@/src/app/store/slices/heroes";
type Props = {
  data: IHeroStats[];
};

const Hero = ({ data = [] }: Props) => {
  //   const dispatch = useDispatch();
  //   dispatch(setItems(data));

  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(setItems(data));
  }, []);
  return (
    <>
      <div className={styles.main}>
        {!!data.length &&
          data.map((el) => (
            <Link key={el.id} href={`/heroes/${el.id}`}>
              <div className={styles.item}>
                <img src={`https://api.opendota.com${el.img}`} alt="" />
                <div className={styles.mask}></div>
                <span>{el.localized_name}</span>
                <div className={styles.tags}>
                  {el.pro_pick > 200 ? <Star classs={styles.pick} /> : ""}
                  {el.pro_win > 200 ? <Star classs={styles.win} /> : ""}
                </div>
              </div>
            </Link>
          ))}
      </div>
    </>
  );
};
export default Hero;
