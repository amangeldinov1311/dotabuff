import React, { useRef } from "react";
import {
  Durations,
  IHeroRank,
  IHeroStats,
  IRankings,
  Match,
  MatchDurations,
  Result,
} from "@/src/types";
import { IDetailHero } from "@/src/types";
import styles from "./styles.module.scss";
import Tabs from "@/src/app/components/Tabs";
import Rankings from "../Rankings";
import Bench from "../Bench";
import Matchups from "../Matchups";
import Duration from "../Duration";
import { useSelector } from "react-redux";
import { CSSTransition } from "react-transition-group";
// type Props = {
//   data: IHeroStats;
// };
type result = {
  result: Result;
};
type rankings = {
  rankings: IHeroRank[];
};
type Props = {
  data: IHeroStats & IRankings & Result & Match & MatchDurations & any;
};

type Attr = {
  atr: number;
  gain: number;
};
type Value = {
  name: string;
  value: string | number;
};
type More = Value[];


function HeroDetail({ data }: Props) {
 

  const overall: IHeroStats = data[0 as unknown as keyof typeof data];
  const rankings: IRankings[] = data[1 as unknown as keyof typeof data];
  const bench: Result = data[2 as unknown as keyof typeof data];
  const matchups: Match = data[4 as unknown as keyof typeof data];
  const durations: MatchDurations = data[3 as unknown as keyof typeof data];

  const [showMore, setShowMore] = React.useState(false);

  const [attrItems, setAttrItems] = React.useState<Attr[]>([]);
  const [moreItems, setMoreItems] = React.useState<More[]>([]);



 
  const attr: Attr[] = [];
  const more: More[] = [];

  React.useEffect(() => {
    if (overall) {
      attr.push(
        { atr: overall.base_str, gain: overall.str_gain },
        { atr: overall.base_agi, gain: overall.agi_gain },
        { atr: overall.base_int, gain: overall.int_gain }
      );
      setAttrItems(attr);
      more.push(
        [
          {
            name: "Базовая атака:",
            value: `${overall.base_attack_min} - ${overall.base_attack_max}`,
          },
          {
            name: "Дальность атаки:",
            value: overall.attack_range,
          },
          {
            name: "Скорость атаки:",
            value: overall.base_attack_time,
          },
        ],
        [
          {
            name: "Здоровье:",
            value: overall.base_health,
          },
          {
            name: "Восстановление здоровья:",
            value: overall.base_health_regen,
          },
          {
            name: "Мана:",
            value: overall.base_mana,
          },
          {
            name: "Восстановление маны:",
            value: overall.base_mana_regen,
          },
        ],
        [
          { name: "Базовая броня:", value: overall.base_armor },
          { name: "Сопротивление магии:", value: "-" },
          { name: "Скорость передвижения:", value: overall.move_speed },
          { name: "Скорость поворота:", value: "-" },
        ],
        [
          { name: "КОЛИЧЕСТВО НОГ:", value: overall.legs },
          {
            name: "ДОСТУПНО В CM:",
            value: `${overall.cm_enabled ? "да" : "нет"}`,
          },
        ]
      );
      setMoreItems(more);
    }
  }, [overall]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.top}>
        <img
          className={styles.img}
          src={`https://api.opendota.com${overall.img}`}
          alt=""
        />
        <div className={styles.shell}>
          <div className={styles.left}>
            <img
              className={styles.logo}
              src={`https://api.opendota.com${overall.img}`}
              alt=""
            />
            <div className={styles.desc}>
              <div className={styles.name}>{overall.localized_name}</div>
              <span className={styles.range}>{overall.attack_type} - </span>
              {overall.roles?.map((el, i) => (
                <span key={i} className={styles.role}>
                  {el},{" "}
                </span>
              ))}
            </div>
          </div>
          <div className={styles.right}>
            <div className={styles.attr}>
              {attrItems.map((el, i: number) => (
                <div key={i} className={styles.attrItem}>
                  <div
                    className={`${
                      i === 0
                        ? styles.red
                        : i === 1
                        ? styles.green
                        : styles.blue
                    } ${styles.before}`}
                  ></div>
                  <span className={`${data.primary_attr} ${styles.str} `}>
                    {el.atr} + {el.gain}
                  </span>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
      <div className={styles.more}>
        <span onClick={() => setShowMore(!showMore)} className={styles.show}>
          {showMore ? "Скрыть подробности " : "Показать подробности"}
        </span>
        <CSSTransition in={showMore} timeout={5000} classNames="an">
          <div>
            {showMore ? (
              <div className={styles.full}>
                {moreItems.map((el, i) => (
                  <div key={i} className={styles.fullItem}>
                    {el.map((item, i: number) => (
                      <div key={i}>
                        <span
                          className={`${(i + 1) % 2 === 0 ? styles.back : ""} ${
                            styles.rowItem
                          }`}
                        >
                          {item.name}
                          <span className={styles.num}>{item.value}</span>{" "}
                        </span>
                      </div>
                    ))}
                  </div>
                ))}
              </div>
            ) : (
              ""
            )}
          </div>
        </CSSTransition>
      </div>
      <Tabs>
        <div title="РАНГИ">
          <Rankings data={rankings} />
        </div>
        <div title="БЕНЧМАРКИ">
          <Bench data={bench} />
        </div>
        <div title="ДЛИТЕЛЬНОСТЬ">
          <Duration data={durations} />
        </div>
        <div title="ПОДБОРЫ">
          <Matchups data={matchups} />
        </div>
     
      </Tabs>
    </div>
  );
}

export default HeroDetail;
