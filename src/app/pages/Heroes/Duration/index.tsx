import { BarChart } from "@/src/app/components/charts/bar";
import { MatchDurations } from "@/src/types";
import React from "react";
import styles from "./styles.module.scss";
type Props = {
  data: MatchDurations;
};
function Duration({ data }: Props) {
  data;
  return (
    <div className={styles.wrapper}>
      <BarChart data={data} />
    </div>
  );
}

export default Duration;
