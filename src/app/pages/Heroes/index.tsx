import React from "react";
import Heroes from "@/src/app/pages/Heroes/Hero";
import Graphic from "../../components/Graphic";
import Star from "../../icons/Star";
import styles from "./styles.module.scss";
import Hero from "@/src/app/pages/Heroes/Hero";
import { IHeroStats } from "@/src/types";
type Props = {
  data: IHeroStats[];
};
const HeroesPage = ({ data }: Props) => {
	
  return (
    <div className={styles.main}>
      {/* <div className={styles.zag}>
        <h1>Герои</h1>
        <span>Все герои</span>
      </div> */}
      {/* <Graphic /> */}
      <div className={styles.title}>
        <h4>Все герои</h4>
        <div className={styles.params}>
          <div className={styles.item}>
            <div className={styles.star}>
              <Star />
            </div>
            Высокая частота пика
          </div>
          <div className={styles.item}>
            <div className={styles.star}>
              <Star />
            </div>
            Высокая доля побед
          </div>
        </div>
      </div>
      {data && <Hero data={data} />}
    </div>
  );
};
export default HeroesPage;
