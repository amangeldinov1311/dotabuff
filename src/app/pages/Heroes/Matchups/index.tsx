import ArrowRight from "@/src/app/icons/ArrowRight";
import { IHeroStats, Match } from "@/src/types";
import Link from "next/link";
import React from "react";
import { useSelector } from "react-redux";
import styles from "./styles.module.scss";
import ProgressBar from "react-bootstrap/ProgressBar";
import { useRouter } from "next/router";
type Props = {
  data: Match;
  //   sortData: Function;
};
// type State = {

//   heroes: IHeroStats[];
// };
function Matchups({ data }: Props) {
  const [combData, setCombData] = React.useState(data);
  const [sortDirect, setSortDirect] = React.useState(true);
  const { items } = useSelector((state: any) => state.heroes);
  const router = useRouter();
  const { id } = router.query;
  const newData = data.map((item) => ({
    ...item,
    winRate: Math.round((item.wins / item.games_played) * 100),
  }));

  React.useEffect(() => {
    let result = newData.map((item) => {
      let { hero_id, games_played, wins, winRate } = item;
      //
      let { localized_name, img } = items.reduce(
        (acc, data) => {
          if (data.hero_id == hero_id) {
            acc.localized_name += data.localized_name;
            acc.img += data.img;
          }
          return acc;
        },
        { localized_name: "", img: "" }
      );
      return { hero_id, games_played, wins, winRate, localized_name, img };
    });
    setCombData(result);
  }, [id]);

  const max = combData.reduce((acc, curr) =>
    acc.games_played > curr.games_played ? acc : curr
  );
  const sortData = (field) => {
    const copyData = combData.concat();
    let sortData;
    if (sortDirect) {
      sortData = copyData.sort((a, b) => {
        return a[field] > b[field] ? 1 : -1;
      });
    } else {
      sortData = copyData.sort((a, b) => {
        return a[field] < b[field] ? 1 : -1;
      });
    }
    setCombData(sortData);
    setSortDirect(!sortDirect);
  };
  return (
    <>
      <table className={styles.table}>
        <thead>
          <tr>
            <th>Герой</th>
            <th onClick={() => sortData("games_played")}>Игры</th>
            <th onClick={() => sortData("winRate")}>% Побед</th>
          </tr>
        </thead>
        <tbody>
          {combData &&
            combData.map((el, i) => (
              <tr key={el.hero_id}>
                <td>
                  {" "}
                  <div className={styles.hero}>
                    <img src={`https://api.opendota.com${el.img}`} alt="" />
                    <Link className={styles.link} href={`${el.hero_id}`}>
                      <div className={styles.text}>
                        <span className={styles.name}>
                          {el.localized_name}
                          <ArrowRight />
                        </span>
                      </div>
                    </Link>
                  </div>
                </td>
                <td>
                  {" "}
                  <div className={styles.wrapper}>
                    {el.games_played}
                    <ProgressBar max={max.games_played} now={el.games_played} />
                  </div>
                </td>
                <td>
                  <div className={styles.wrapper}>
                    {`${el.winRate}%`}
                    <ProgressBar max={100} now={el.winRate} />
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
}

export default Matchups;
