import Link from "next/link";
import React from "react";
import { useSelector } from "react-redux";
import Pagination from "../../components/Pagination";
import ArrowRight from "../../icons/ArrowRight";
import styles from "./styles.module.scss";
import { Planets } from "react-preloaders";
function SearchPage() {
  // type State={
  // 	players:Ipla
  // }
  const { items, loading } = useSelector((state: any) => state.players);
  console.log(items);

  const [currentPage, setCurrentPage] = React.useState<number>(1);
  const [playersPerPage] = React.useState<number>(10);

  const lastPlayerIndex = currentPage * playersPerPage;
  const firstPlayersIndex = lastPlayerIndex - playersPerPage;
  const currentPlayer = items.slice(firstPlayersIndex, lastPlayerIndex);
  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);
  console.log(currentPlayer);
  if (!items.length) {
    return <></>;
  }
  if (!loading) {
    return (
      <div className={styles.wrapper}>
        <div className={styles.title}>
          Игроки <span>{`${items.length} результаты`}</span>
        </div>
        <table className={styles.table}>
          <thead>
            <tr>
              <td>Имя</td>
            </tr>
          </thead>
          <tbody>
            {currentPlayer &&
              currentPlayer.map((item, i) => (
                <tr key={i}>
                  <td>
                    <img src={item.avatarfull} alt="" />
                    <Link className={styles.link} href="#">
                      <span className={styles.name}>
                        {item.personaname}
                        <ArrowRight />
                      </span>
                    </Link>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        <Pagination
          playersPerPage={playersPerPage}
          totalPlayers={items.length}
          paginate={paginate}
        />
      </div>
    );
  } else {
    return <Planets />;
  }
}

export default SearchPage;
