import React from "react";
import Nav from "../Nav";
import styles from "./styles.module.scss";
import Link from "next/link";
import Search from "./Search";
const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.shell}>
        <Link href="/">
          <div className={styles.logo}>Dotabuff</div>
        </Link>
        <Nav />
      </div>
      <Search />
    </div>
  );
};
export default Header;
