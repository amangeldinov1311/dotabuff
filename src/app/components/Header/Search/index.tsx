import { useGetPlayersQuery } from "@/src/app/store/services/players";
import { fetchPlayers, setItems } from "@/src/app/store/slices/players";
import { AppDispatch } from "@/src/app/store/store";
import { useRouter } from "next/router";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import styles from "./styles.module.scss";
function Search() {
  const router = useRouter();
  const [term, setTerm] = React.useState<string>("");
  const dispatch = useDispatch<AppDispatch>();
  //   const data = useGetPlayersQuery("");
  const submitHandler = (e) => {
    e.preventDefault();

    dispatch(fetchPlayers(term));
    router.push("/search");
  };

  return (
    <div>
      <form className={styles.form} onSubmit={submitHandler}>
        <input
          type="text"
          value={term}
          onChange={(e) => setTerm(e.target.value)}
          placeholder="Поиск по имени игрока, ID матча..."
        />
        <button type="submit">Искать</button>
      </form>
    </div>
  );
}

export default Search;
