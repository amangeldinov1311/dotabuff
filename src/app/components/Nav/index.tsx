import Link from "next/link";
import React from "react";
import styles from "./styles.module.scss";
const Nav = () => {
  return (
    <ul className={styles.nav}>
      <Link className={styles.link} href={"/heroes"}>
        <li>Герои</li>
      </Link>
    </ul>
  );
};
export default Nav;
