import React from "react";
import styles from "./styles.module.scss";
type Props = {
  playersPerPage: number;
  totalPlayers: number;
  paginate: Function;
};
function Pagination({ playersPerPage, totalPlayers, paginate }: Props) {
  const pageNumbers: number[] = [];
  const [active, setActive] = React.useState<number>(0);
  for (let i = 1; i <= Math.ceil(totalPlayers / playersPerPage); i++) {
    pageNumbers.push(i);
  }
  console.log(pageNumbers);
  return (
    <div className={styles.wrapper}>
      <ul className={styles.list}>
        {pageNumbers.map((number, i) => (
          <li key={number}>
            <span
              className={`${active === i ? styles.active : ""} ${styles.item} `}
              onClick={() => {
                paginate(number);
                setActive(i);
              }}
            >
              {number}
            </span>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Pagination;
