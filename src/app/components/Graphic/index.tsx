import React from "react";
import Plus from "../../icons/Plus";
import styles from "./styles.module.scss";
const Graphic = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.zag}>
        <h4>Графики</h4>
        <span>Наибольшие изменения на этой неделе</span>
      </div>
      <span className={styles.more}>
        <Plus />
        Больше
      </span>
    </div>
  );
};
export default Graphic;
