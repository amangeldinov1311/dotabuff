import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend,
  Scriptable,
} from "chart.js";
import { Line } from "react-chartjs-2";
import { ScriptableContext } from "chart.js";
import { Chart as ReactChart } from "react-chartjs-2";
import { faker } from "@faker-js/faker";
import { PerMin } from "@/src/types";
type Props = {
  data: PerMin[];
  color: string;
  gradient1: string;
  gradient2: string;
};
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);

export const options = {
  responsive: false,
  maintainAspectRatio: false,
  plugins: {
    legend: {
      display: false,
    },
  },
};



export function AreaChart(props: Props) {
  const { data } = props;
  const { color } = props;
  const { gradient1 } = props;
  const { gradient2 } = props;

  const value = {
    labels: data.map((el) => `${el.percentile * 100}%`),
    datasets: [
      {
        fill: true,
        label: "Заработано в минуту",
        data: data.map((el) => el.value),
        borderColor: color,
        backgroundColor: (context: ScriptableContext<"line">) => {
          const ctx = context.chart.ctx;
          const gradient = ctx.createLinearGradient(0, 0, 0, 250);
          gradient.addColorStop(1, gradient1);
          gradient.addColorStop(.3, gradient2);
          return gradient;
        },
      },
    ],
  };
  return <Line options={options} data={value} width="380px" height="300px" />;
}
