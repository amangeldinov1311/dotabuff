import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { MatchDurations } from "@/src/types";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      display: false,
    },
    title: {
      display: false,
      text: "Chart.js Bar Chart",
    },
  },
};

type Props = {
  data: MatchDurations;
};

// export function BarChart(props: Props) {
//   const { data } = props;
//   const games = data.map((el) => el.games_played);
//   const win = data.map((el) => el.wins);
//   const min = data.map((el) => el.duration_bin / 60);

//   let merged = min.map((item, i) => {
//     return {
//       duration: min[i],
//       games: games[i],
//       wins: win[i],
//     };
//   });
//   (merged);
//   const dataSort = merged.sort((a, b) => {
//     return a.duration - b.duration;
//   });
//   (dataSort);
//   const value = {
//     	labels: dataSort.map((el) => el.duration),
//     datasets: [
//       {
//         label: "Матчей",
//         data: dataSort.map((el) => el.games),
//         backgroundColor: "rgba(229, 182, 22, 0.5)",
//       },
//     ],
//   };
//   return <Bar options={options} data={value} />;
// }
export function BarChart(props: Props) {
  const { data } = props;
  const games = data.map((el) => el.games_played);
  const win = data.map((el) => el.wins);
  const min = data.map((el) => el.duration_bin / 60);

  let merged = min.map((item, i) => {
    return {
      duration: min[i],
      games: games[i],
      wins: win[i],
    };
  });

  const dataSort = merged.sort((a, b) => {
    return a.duration - b.duration;
  });

  const value = {
    labels: dataSort.map((el) => `${el.duration} Минут`),
    datasets: [
      {
        label: "Матчей",
        data: dataSort.map((el) => el.games),
        backgroundColor: "rgba(229, 182, 22, 0.5)",
      },
    ],
  };
  return <Bar options={options} data={value} />;
}
