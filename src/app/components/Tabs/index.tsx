import React, { useState } from "react";
import styles from "./styles.module.scss";

type Props = {
  title: string;
  index: number;
  setDefaultIndex: Function;
  defaultIndex: number;
};
type Propss = {
  children: React.ReactElement[];
  onClickTabItem?: Function;
};
export function Tab({ title, index, defaultIndex, setDefaultIndex }: Props) {
  const clickToTab = (i: number) => {
    setDefaultIndex(i);
  };
  return (
    <li
      className={
        index === defaultIndex
          ? `${styles.activeTabListItem}`
          : `${styles.tabListItem}`
      }
      onClick={() => clickToTab(index)}
    >
      <span>{title}</span>
    </li>
  );
}

export default function Tabs({ children }: Propss) {
  const [defaultIndex, setDefaultIndex] = React.useState(0);
  return (
    <div className={`${styles.tabsComponents} shell`}>
      <div className={`${styles.tabsLinks} tabsLinks`}>
        <ol className={styles.tabsList}>
          {children.map((child: any, index: number) => (
            <Tab
              key={child.props.title}
              title={child.props.title}
              index={index}
              setDefaultIndex={setDefaultIndex}
              defaultIndex={defaultIndex}
            />
          ))}
        </ol>
        <span className={styles.tabLine}></span>
      </div>

      <div className={`${styles.tabsContent} content`}>
        {children.map((child: React.ReactElement, index: number) => {
          if (index !== defaultIndex) return undefined;
          return child.props.children;
        })}
      </div>
    </div>
  );
}

// import React from "react";
// import styles from "./styles.module.scss";
// type Props = {
//   activeTab: string;
//   title: string;
//   onClick?: Function;
//   index: number;
// };
// type Propss = {
//   children: any;
//   onClickTabItem?: Function;
// };
// export function Tab({ title, activeTab, onClick }: Props) {
//   const click = () => {
//     onClick(title);
//     {
//
//     }
//   };
//   return (
//     <li
//       className={
//         activeTab === title
//   ? `${styles.activeTabListItem}`
//           : `${styles.tabListItem}`
//       }
//       onClick={click}
//     >
//       <span>{title}</span>
//     </li>
//   );
// }

// export default function Tabs({ children, onClickTabItem }: Propss) {
//   const [activeTab, setActiveTab] = React.useState(children[0].props.title);

//   onClickTabItem = (tab: any) => {
//     setActiveTab(tab);
//   };
//   return (
//     <div className={styles.tabsComponents}>
//       <div className={styles.tabsLinks}>
//         <ol className={styles.tabsList}>
//           {children.map((child: any) => (
//             <Tab
//               index={child.props.index}
//               activeTab={activeTab}
//               key={child.props.title}
//               title={child.props.title}
//               onClick={onClickTabItem}
//             />
//           ))}
//         </ol>
//         <span className={styles.tabLine}></span>
//       </div>
//       <div className={styles.tabsContent}>
//         {children.map((child: { props: { title: any; children: any } }) => {
//           if (child.props.title !== activeTab) return undefined;
//           return child.props.children;
//         })}
//       </div>
//     </div>
//   );
// }
