import Header from "../Header";
import styles from "./styles.module.scss";
type Props = {
  children: React.ReactElement;
};
function Wrapper({ children }: Props) {
  return (
    <>
      <div className={styles.wrapper}>
        <Header />
        {children}
        {/* <Footer /> */}
      </div>
    </>
  );
}

export default Wrapper;
