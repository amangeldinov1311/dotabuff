
export interface IHeroStats {
  id: number;
  name: string;
  localized_name: string;
  primary_attr: string;
  attack_type: string;
  roles: string[];
  img: string;
  icon: string;
  base_health: number;
  base_health_regen: number;
  base_mana: number;
  base_mana_regen: number;
  base_armor: number;
  base_mr: number;
  base_attack_min: number;
  base_attack_max: number;
  base_str: number;
  base_agi: number;
  base_int: number;
  str_gain: number;
  agi_gain: number;
  int_gain: number;
  attack_range: number;
  projectile_speed: number;
  attack_rate: number;
  base_attack_time: number;
  attack_point: number;
  move_speed: number;
  turn_rate?: any;
  cm_enabled: boolean;
  legs: number;
  day_vision: number;
  night_vision: number;
  hero_id: number;
  turbo_picks: number;
  turbo_wins: number;
  pro_ban: number;
  pro_win: number;
  pro_pick: number;
  "1_pick": number;
  "1_win": number;
  "2_pick": number;
  "2_win": number;
  "3_pick": number;
  "3_win": number;
  "4_pick": number;
  "4_win": number;
  "5_pick": number;
  "5_win": number;
  "6_pick": number;
  "6_win": number;
  "7_pick": number;
  "7_win": number;
  "8_pick": number;
  "8_win": number;
  null_pick: number;
  null_win: number;
}
export interface IDetailHero {
  id: number;
  name: string;
  localized_name: string;
  primary_attr: string;
  attack_type: string;
  roles: string[];
  legs: number;
}

export interface IHeroRank {
  hero_id: number;
  rankings: IRankings[];
}
export interface IRankings {
  account_id: number;
  score: number;
  personaname: string;
  name: any;
  avatar: string;
  last_login: string;
  rank_tier: number;
}
export interface IBench {
  hero_id: number;
  result: Result;
}
export interface Result {
  gold_per_min: PerMin[];
  xp_per_min: PerMin[];
  kills_per_min: PerMin[];
  last_hits_per_min: PerMin[];
  hero_damage_per_min: PerMin[];
  hero_healing_per_min: PerMin[];
  tower_damage: PerMin[];
  stuns_per_min: PerMin[];
  lhten: PerMin[];
}
export interface PerMin {
  percentile: number;
  value: number;
}
export type Match = Ups[];

export interface Ups {
  hero_id: number;
  games_played: number;
  wins: number;
  img?: string;
  localized_name?: string;
  winRate?: number;
}

export type MatchDurations = Durations[];

export interface Durations {
  duration_bin: number;
  games_played: number;
  wins: number;
}
export type IPlayer = Player[];
export interface Player {
  account_id: number;
  personaname: string;
  avatarfull: string;
  last_match_time?: string;
  similarity: number;
}
