import HeroesPage from "@/src/app/pages/Heroes";
import { useGetAllHeroesQuery } from "@/src/app/store/services/heroes";
import { setItems } from "@/src/app/store/slices/heroes";
import React from "react";
import { useDispatch } from "react-redux";

function Heroes() {
  const { data, error, isLoading } = useGetAllHeroesQuery("");

  if (isLoading) {
    return <>Идет загрузка</>;
  }
  if (error) {
    return <>Ошибка!</>;
  }
  if (data) {
    return <HeroesPage data={data} />;
  }
}

export default Heroes;
