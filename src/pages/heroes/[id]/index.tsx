import HeroDetail from "@/src/app/pages/Heroes/HeroDetail";
import { useGetAllHeroesQuery } from "@/src/app/store/services/heroes";
import { useGetHeroesMatchupsQuery } from "@/src/app/store/services/heroesDetail";
import { useGetRankingsQuery } from "@/src/app/store/services/rankings";
import { useGetHeroesDurationsQuery } from "@/src/app/store/services/heroesDetail";
import { useRouter } from "next/router";
import React from "react";
import { useGetAllBenchQuery } from "@/src/app/store/services/bench";
import { useSelector, useDispatch } from "react-redux";
import { setLoading } from "@/src/app/store/slices/heroes";
function hero() {
  const router = useRouter();
  const { id } = router.query;
  const dispatch = useDispatch();
  if (id) {
    const { data, isLoading: dataLoading } = useGetAllHeroesQuery("");
    const dataItem = !!data && data.find((el) => Number(el.id) === Number(id));

    const { data: rankings, isLoading: rankLoading } = useGetRankingsQuery(
      Number(id)
    );
    const { data: bench, isLoading: benchLoading } = useGetAllBenchQuery(
      Number(id)
    );
    const { data: durations, isLoading: durationsLoading } =
      useGetHeroesDurationsQuery(Number(id));
    const { data: matchups, isLoading: matchupsLoading } =
      useGetHeroesMatchupsQuery(Number(id));
    const oData = [];
    oData.push(
      dataItem,
      rankings?.rankings,
      bench?.result,
      durations,
      matchups
    );

    return (
      rankings &&
      bench &&
      dataItem &&
      durations &&
      matchups && <HeroDetail data={oData} />
    );
  }
}

export default hero;
