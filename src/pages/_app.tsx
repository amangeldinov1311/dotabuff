import "../app/styles/main.scss";
import type { AppProps } from "next/app";
import Wrapper from "../app/components/Wrapper";
import { Provider } from "react-redux";
import { store } from "../app/store/store";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Provider store={store}>
        <Wrapper>
          <Component {...pageProps} />
        </Wrapper>
      </Provider>
    </>
  );
}
