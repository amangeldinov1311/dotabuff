import SearchPage from "@/src/app/pages/Search";
import React from "react";

function Search() {
  return <SearchPage />;
}

export default Search;
